import 'dart:async';
import 'package:flutter/material.dart';

import 'package:flutter_profile/page/edit_profile.dart';
import 'package:flutter_profile/helpers/dbHelper.dart';
import 'package:flutter_profile/models/profile_data.dart';
import 'package:sqflite/sqflite.dart';

class Profile extends StatefulWidget {
  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  DbHelper dbHelper = DbHelper();
  int count = 0;
  Future<List<Profile_model>> contactList;

  @override
  Widget build(BuildContext context) {
    // if (contactList == null) {
    //   contactList = List<Profile_model>();
    // }

    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Indashafa_',
            style: TextStyle(fontSize: 18),
          ),
          leading: IconButton(
            icon: Icon(Icons.add),
            onPressed: () {},
            color: Colors.black,
            iconSize: 30.0,
          ),
          actions: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: IconButton(
                icon: Icon(Icons.menu),
                onPressed: () {},
                color: Colors.black,
                iconSize: 30.0,
              ),
            )
          ],
        ),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.all(15),
              margin: EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey,
                  width: 0.1,
                ),
              ),
              child: Center(
                child: Text(
                  'See Covid-19 Business Resources',
                  style: TextStyle(color: Colors.cyan, fontSize: 12),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10),
            ),
            Row(
              children: [
                kotakPhoto(
                    'https://cdn.akurat.co/unsafe/750x499/http://image.akurat.co/images/uploads/images/akurat_20200221045728_s6ek6v.jpg'),
                kotak('0', 'Posts', 16, 12),
                kotak('124', 'Followers', 16, 12),
                kotak('166', 'Following', 16, 12)
              ],
            ),
            Container(
              alignment: Alignment(-0.98, 0.0),
              padding: EdgeInsets.symmetric(horizontal: 16),
              margin: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Text(
                    'Safarotul Indari',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  new Text(
                    'Informatic Engineering',
                    style: TextStyle(fontSize: 13),
                    textAlign: TextAlign.justify,
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlineButton(
                    padding: EdgeInsets.symmetric(horizontal: 26),
                    onPressed: () async {
                      var profile = await navigateToEntryForm(context, null);
                      if (profile != null) addContact(profile);
                    },
                    child: Text(
                      'Edit Profile',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  button('Promotions'),
                  button('Insights')
                ],
              ),
            ),
          ],
        ));
  }

  // kumpulan fungsi fungsi
  //

  void query() async {}

  void nama() {
    print(contactList);
  }

  Future<Profile_model> navigateToEntryForm(
      BuildContext context, Profile_model profile_model) async {
    var result = await Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) {
      return EditForm(profile_model);
    }));
    return result;
  }

  //buat contact
  void addContact(Profile_model object) async {
    int result = await dbHelper.insert(object);
    if (result > 0) {
      print('data berhasil ditambah');
    }
  }

  Widget kotak(String teks1, String teks2, double size1, double size2) {
    return Container(
      child: Column(
        children: [
          Text(
            teks1,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: size1),
          ),
          Text(teks2,
              style: TextStyle(fontSize: size2, fontWeight: FontWeight.bold))
        ],
      ),
      margin: EdgeInsets.all(15.5),
    );
  }

  Widget kotakPhoto(String url) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 0, 30, 0),
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        // color: Colors.teal,
        borderRadius: BorderRadius.circular(100.0),
        image: DecorationImage(
          image: NetworkImage(url),
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget button(String teks) {
    return Padding(
        padding: EdgeInsets.all(5),
        child: OutlineButton(
          padding: EdgeInsets.symmetric(horizontal: 26),
          onPressed: () {},
          child: Text(
            teks,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ));
  }
}
