import 'package:flutter/material.dart';
import 'package:flutter_profile/models/profile_data.dart';
import 'package:flutter/cupertino.dart';

class EditForm extends StatefulWidget {
  final Profile_model profile_model;

  EditForm(this.profile_model);

  @override
  FormState createState() => FormState(this.profile_model);
}

class FormState extends State<EditForm> {
  Profile_model profile_model;

  FormState(this.profile_model);

  TextEditingController usernameController = TextEditingController();
  TextEditingController namaController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController gambarProfileController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    if (profile_model != null) {
      usernameController.text = profile_model.username;
      namaController.text = profile_model.nama;
      deskripsiController.text = profile_model.deskripsi;
      gambarProfileController.text = profile_model.gambarProfile;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Edit Profile',
          style: TextStyle(fontSize: 18),
        ),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.black,
          iconSize: 30.0,
        ),
        actions: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: IconButton(
              icon: Icon(Icons.check),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) => CupertinoAlertDialog(
                          title: Text('Profile Changed'),
                          content: Text('Do you want to save your edits?'),
                          actions: [
                            CupertinoDialogAction(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Cancel'),
                            ),
                            CupertinoDialogAction(
                              onPressed: () {
                                if (profile_model == null) {
                                  // tambah data
                                  profile_model = Profile_model(
                                      usernameController.text,
                                      namaController.text,
                                      deskripsiController.text,
                                      gambarProfileController.text);
                                } else {
                                  // ubah data
                                  profile_model.username =
                                      usernameController.text;
                                  profile_model.nama = namaController.text;
                                  profile_model.deskripsi =
                                      deskripsiController.text;
                                  profile_model.gambarProfile =
                                      gambarProfileController.text;
                                }
                                Navigator.pop(context, profile_model);
                              },
                              child: Text('Save'),
                            )
                          ],
                        ),
                    barrierColor: Colors.black.withOpacity(0.7),
                    barrierDismissible: false);

                // cek apakah didalam
                // profile model ada isinya atau tidak
                showDialog(
                    context: context,
                    builder: (_) => AlertDialog(
                          title: Text('Profile Changed'),
                          content: Text('Do you want to save your edits?'),
                          actions: [
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('No'),
                            ),
                            FlatButton(
                              onPressed: () {
                                if (profile_model == null) {
                                  // tambah data
                                  profile_model = Profile_model(
                                      usernameController.text,
                                      namaController.text,
                                      deskripsiController.text,
                                      gambarProfileController.text);
                                } else {
                                  // ubah data
                                  profile_model.username =
                                      usernameController.text;
                                  profile_model.nama = namaController.text;
                                  profile_model.deskripsi =
                                      deskripsiController.text;
                                  profile_model.gambarProfile =
                                      gambarProfileController.text;
                                }
                                Navigator.pop(context, profile_model);
                              },
                              child: Text('Yes'),
                            )
                          ],
                          elevation: 0,
                          backgroundColor: Colors.white,
                        ),
                    barrierColor: Colors.black.withOpacity(0.7),
                    barrierDismissible: false);
              },
              color: Colors.black,
              iconSize: 30.0,
            ),
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
        child: ListView(
          children: <Widget>[
            // form
            // username
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
              child: TextField(
                maxLines: null,
                controller: usernameController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelStyle: new TextStyle(color: Colors.black38),
                    labelText: 'Id',
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black87)),
                    border: UnderlineInputBorder()),
                onChanged: (value) {
                  if (value.isEmpty) {
                    return 'id tidak boleh kosong';
                  }
                },
              ),
            ),

            // nama
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
              child: TextField(
                maxLines: null,
                controller: namaController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelStyle: new TextStyle(color: Colors.black38),
                    labelText: 'Nama',
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black87)),
                    border: UnderlineInputBorder()),
                onChanged: (value) {
                  if (value.isEmpty) {
                    return 'Nama tidak boleh kosong';
                  }
                },
              ),
            ),

            // deskripsi
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
              child: TextField(
                maxLines: null,
                controller: deskripsiController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelStyle: new TextStyle(color: Colors.black38),
                    labelText: 'Deskripsi',
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black87)),
                    border: UnderlineInputBorder()),
                onChanged: (value) {
                  if (value.isEmpty) {
                    return 'Deskripsi tidak boleh kosong';
                  }
                },
              ),
            ),

            // Photo
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 10.0),
              child: TextField(
                maxLines: null,
                controller: gambarProfileController,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelStyle: new TextStyle(color: Colors.black38),
                    labelText: 'Photo Url',
                    focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black87)),
                    border: UnderlineInputBorder()),
                onChanged: (value) {},
              ),
            ),

            Padding(
              padding: EdgeInsets.symmetric(vertical: 13.0, horizontal: 10.0),
              child: Text(
                "Provide your personal information, even if the account is used for business, a pet or something else. This won't be a part of your public profile",
                style: TextStyle(color: Colors.grey[400], fontSize: 13),
              ),
            )
          ],
        ),
      ),
    );
  }
}
