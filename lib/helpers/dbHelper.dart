import 'package:sqflite/sqflite.dart';
import 'dart:async';
//mendukug pemrograman asinkron
import 'dart:io';
//bekerja pada file dan directory
import 'package:path_provider/path_provider.dart';
import 'package:flutter_profile/models/profile_data.dart';

//kelass Dbhelper
class DbHelper {
  static DbHelper _dbHelper;
  static Database _database;

  DbHelper._createObject();
  static final DbHelper instance = DbHelper._createObject();
  factory DbHelper() {
    if (_dbHelper == null) {
      _dbHelper = DbHelper._createObject();
    }
    return _dbHelper;
  }

  Future<Database> initDb() async {
    //untuk menentukan nama database dan lokasi yg dibuat
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path + 'profileData.db';

    //create, read databases
    var todoDatabase = openDatabase(path, version: 1, onCreate: _createDb);

    //mengembalikan nilai object sebagai hasil dari fungsinya
    return todoDatabase;
  }

  //buat tabel baru dengan nama contact
  void _createDb(Database db, int version) async {
    await db.execute('''
      CREATE TABLE profileData (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username TEXT,
        nama TEXT,
        deskripsi TEXT,
        gambarProfile TEXT

      )
    ''');
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDb();
    }
    return _database;
  }

  Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await this.database;
    return await db.query('profileData');
  }

  Future<List<Map<String, dynamic>>> select() async {
    Database db = await this.database;
    var mapList = await db.query('profileData', orderBy: 'nama');
    return mapList;
  }

//create databases
  Future<int> insert(Profile_model object) async {
    Database db = await this.database;
    int count = await db.insert('profileData', object.toMap());
    print(count);
    return count;
  }

//update databases
  Future<int> update(Profile_model object) async {
    Database db = await this.database;
    int count = await db.update('profileData', object.toMap(),
        where: 'id=?', whereArgs: [object.id]);
    return count;
  }

//delete databases
  Future<int> delete(int id) async {
    Database db = await this.database;
    int count = await db.delete('profileData', where: 'id=?', whereArgs: [id]);
    return count;
  }

  // delete semua
  Future<int> deleteAll() async {
    Database db = await this.database;
    int count = await db.delete('profileData');
    return count;
  }

  Future<List<Profile_model>> getContactList() async {
    var contactMapList = await select();
    int count = contactMapList.length;
    List<Profile_model> contactList = List<Profile_model>();
    for (int i = 0; i < count; i++) {
      contactList.add(Profile_model.fromMap(contactMapList[i]));
    }
    return contactList;
  }
}
